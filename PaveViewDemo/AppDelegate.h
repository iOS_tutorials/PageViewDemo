//
//  AppDelegate.h
//  PaveViewDemo
//
//  Created by Joan Barrull Ribalta on 21/04/16.
//  Copyright © 2016 com.giria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

