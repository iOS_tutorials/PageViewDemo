//
//  ViewController.h
//  PaveViewDemo
//
//  Created by Joan Barrull Ribalta on 21/04/16.
//  Copyright © 2016 com.giria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContentViewController.h"

@interface ViewController : UIViewController <UIPageViewControllerDataSource>



@property (strong, nonatomic) IBOutlet UIButton *startWalktrough;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;
- (IBAction)startWalkthrough:(id)sender;
@end

