//
//  PageContentViewController.m
//  PaveViewDemo
//
//  Created by Joan Barrull Ribalta on 21/04/16.
//  Copyright © 2016 com.giria. All rights reserved.
//

#import "PageContentViewController.h"

@interface PageContentViewController ()

@end

@implementation PageContentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.backgroundImageView.image = [UIImage imageNamed:self.imageFile];
    self.titleLabel.text = self.titleText;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)closePresssed:(UIButton *)sender {
    
    UIPageViewController *pvc = (UIPageViewController*) self.parentViewController;
    [pvc.view removeFromSuperview];
    [pvc removeFromParentViewController];
    
}
@end
